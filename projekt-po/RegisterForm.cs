﻿using System;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace projekt_po
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            string id = RegisterInputId.Text;
            string password = RegisterInputPassword.Text;

            string hashedPassword = PasswordHandler.CreatePasswordHash(password);

            string newUser = id + " " + hashedPassword + Environment.NewLine;

            string path = "users.txt";

            bool isValidate = true;


            if (id.Length != 3 || !IsNumberString(id))
            {
                IdLabel.Text = "Identyfikator musi składać się z 3 cyfr";
                isValidate = false;
            }
            else
            {
                IdLabel.Text = "";
            }

            string[] fileText = File.ReadAllLines(path);

            foreach (string line in fileText)
            {
                string[] words = line.Split(' ');
                string lineId = words[0];

                if (id == lineId)
                {
                    IdLabel.Text = "Użytkownik z podanym ID już istnieje";
                    isValidate = false;
                }
            }

            if (password.Length < 6)
            {
                PasswordLabel.Text = "Hasło musi składać się z co najmniej 6 znaków";
                isValidate = false;
            }
            else
            {
                PasswordLabel.Text = "";
            }

            if (isValidate)
            {
                File.AppendAllText(path, newUser);
                RedirectToLogin(id);
            }
        }

        private void RegisterToLoginLink_LinkClicked(object sender, EventArgs e)
        {
            RedirectToLogin();
        }

        private void RedirectToLogin(string id = "")
        {
            LoginForm loginForm = new LoginForm(id)
            {
                Width = this.Width,
                Height = this.Height,
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.Location.X, this.Location.Y)
            };
            this.Hide();
            loginForm.ShowDialog();
            this.Close();
        }
        private bool IsNumberString(string text)
        {
            bool result = true;
            foreach (char c in text)
            {
                if (!Char.IsNumber(c))
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
