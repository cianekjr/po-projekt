﻿namespace projekt_po
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RegisterInputPassword = new projekt_po.BorderInput();
            this.RegisterInputId = new projekt_po.BorderInput();
            this.label1 = new System.Windows.Forms.Label();
            this.RegisterButton = new projekt_po.RoundedButton();
            this.ErrorBox = new System.Windows.Forms.Label();
            this.RegisterToLoginLink = new System.Windows.Forms.LinkLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.IdLabel = new System.Windows.Forms.Label();
            this.PasswordLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(665, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 17);
            this.label3.TabIndex = 20;
            this.label3.Text = "Identyfikator";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(715, 258);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 19;
            this.label2.Text = "Hasło";
            // 
            // RegisterInputPassword
            // 
            this.RegisterInputPassword.BackColor = System.Drawing.SystemColors.Highlight;
            this.RegisterInputPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RegisterInputPassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegisterInputPassword.Location = new System.Drawing.Point(462, 222);
            this.RegisterInputPassword.Name = "RegisterInputPassword";
            this.RegisterInputPassword.Size = new System.Drawing.Size(301, 33);
            this.RegisterInputPassword.TabIndex = 18;
            this.RegisterInputPassword.UseSystemPasswordChar = true;
            // 
            // RegisterInputId
            // 
            this.RegisterInputId.BackColor = System.Drawing.SystemColors.Highlight;
            this.RegisterInputId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.RegisterInputId.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegisterInputId.Location = new System.Drawing.Point(462, 134);
            this.RegisterInputId.MaxLength = 3;
            this.RegisterInputId.Name = "RegisterInputId";
            this.RegisterInputId.Size = new System.Drawing.Size(301, 33);
            this.RegisterInputId.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(485, 423);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 18);
            this.label1.TabIndex = 16;
            this.label1.Text = "Masz już konto?";
            // 
            // RegisterButton
            // 
            this.RegisterButton.BackColor = System.Drawing.SystemColors.HighlightText;
            this.RegisterButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RegisterButton.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegisterButton.Location = new System.Drawing.Point(462, 343);
            this.RegisterButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(301, 57);
            this.RegisterButton.TabIndex = 15;
            this.RegisterButton.Text = "Utwórz użytkownika";
            this.RegisterButton.UseVisualStyleBackColor = false;
            this.RegisterButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // ErrorBox
            // 
            this.ErrorBox.AutoSize = true;
            this.ErrorBox.BackColor = System.Drawing.SystemColors.Highlight;
            this.ErrorBox.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ErrorBox.ForeColor = System.Drawing.Color.Red;
            this.ErrorBox.Location = new System.Drawing.Point(359, 332);
            this.ErrorBox.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.ErrorBox.Name = "ErrorBox";
            this.ErrorBox.Size = new System.Drawing.Size(0, 17);
            this.ErrorBox.TabIndex = 14;
            // 
            // RegisterToLoginLink
            // 
            this.RegisterToLoginLink.AutoSize = true;
            this.RegisterToLoginLink.Font = new System.Drawing.Font("Verdana", 10.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegisterToLoginLink.Location = new System.Drawing.Point(628, 423);
            this.RegisterToLoginLink.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.RegisterToLoginLink.Name = "RegisterToLoginLink";
            this.RegisterToLoginLink.Size = new System.Drawing.Size(94, 18);
            this.RegisterToLoginLink.TabIndex = 13;
            this.RegisterToLoginLink.TabStop = true;
            this.RegisterToLoginLink.Text = "Zaloguj się!";
            this.RegisterToLoginLink.Click += new System.EventHandler(this.RegisterToLoginLink_LinkClicked);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(559, 58);
            this.label4.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 32);
            this.label4.TabIndex = 12;
            this.label4.Text = "Witaj!";
            // 
            // IdLabel
            // 
            this.IdLabel.AutoSize = true;
            this.IdLabel.BackColor = System.Drawing.SystemColors.Highlight;
            this.IdLabel.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.IdLabel.ForeColor = System.Drawing.Color.Red;
            this.IdLabel.Location = new System.Drawing.Point(473, 187);
            this.IdLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.IdLabel.Name = "IdLabel";
            this.IdLabel.Size = new System.Drawing.Size(0, 17);
            this.IdLabel.TabIndex = 21;
            // 
            // PasswordLabel
            // 
            this.PasswordLabel.AutoSize = true;
            this.PasswordLabel.BackColor = System.Drawing.SystemColors.Highlight;
            this.PasswordLabel.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PasswordLabel.ForeColor = System.Drawing.Color.Red;
            this.PasswordLabel.Location = new System.Drawing.Point(449, 275);
            this.PasswordLabel.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.PasswordLabel.Name = "PasswordLabel";
            this.PasswordLabel.Size = new System.Drawing.Size(0, 17);
            this.PasswordLabel.TabIndex = 22;
            // 
            // RegisterForm
            // 
            this.AcceptButton = this.RegisterButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1198, 594);
            this.Controls.Add(this.PasswordLabel);
            this.Controls.Add(this.IdLabel);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RegisterInputPassword);
            this.Controls.Add(this.RegisterInputId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RegisterButton);
            this.Controls.Add(this.ErrorBox);
            this.Controls.Add(this.RegisterToLoginLink);
            this.Controls.Add(this.label4);
            this.Name = "RegisterForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private BorderInput RegisterInputPassword;
        private BorderInput RegisterInputId;
        private System.Windows.Forms.Label label1;
        private RoundedButton RegisterButton;
        private System.Windows.Forms.Label ErrorBox;
        private System.Windows.Forms.LinkLabel RegisterToLoginLink;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label IdLabel;
        private System.Windows.Forms.Label PasswordLabel;
    }
}