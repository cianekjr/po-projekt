﻿namespace projekt_po
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radio1_2 = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radio1_1 = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.radio1_0 = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.radio2_0 = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.radio2_1 = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.radio2_2 = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.radio2_3 = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.radio4_0 = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radio4_1 = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.radio4_2 = new System.Windows.Forms.RadioButton();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.radio4_3 = new System.Windows.Forms.RadioButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.radio3_0 = new System.Windows.Forms.RadioButton();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.radio3_1 = new System.Windows.Forms.RadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.radio3_2 = new System.Windows.Forms.RadioButton();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.radio3_3 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.radio8_0 = new System.Windows.Forms.RadioButton();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.radio8_1 = new System.Windows.Forms.RadioButton();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.radio8_2 = new System.Windows.Forms.RadioButton();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.radio8_3 = new System.Windows.Forms.RadioButton();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.radio7_0 = new System.Windows.Forms.RadioButton();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.radio7_1 = new System.Windows.Forms.RadioButton();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.radio7_2 = new System.Windows.Forms.RadioButton();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.radio7_3 = new System.Windows.Forms.RadioButton();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.radio6_0 = new System.Windows.Forms.RadioButton();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.radio6_1 = new System.Windows.Forms.RadioButton();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.radio6_2 = new System.Windows.Forms.RadioButton();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.radio6_3 = new System.Windows.Forms.RadioButton();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.radio5_0 = new System.Windows.Forms.RadioButton();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.radio5_1 = new System.Windows.Forms.RadioButton();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.radio5_2 = new System.Windows.Forms.RadioButton();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.radio5_3 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radio1_3 = new System.Windows.Forms.RadioButton();
            this.Input4 = new System.Windows.Forms.TextBox();
            this.Input5 = new System.Windows.Forms.TextBox();
            this.Input6 = new System.Windows.Forms.TextBox();
            this.Input1 = new System.Windows.Forms.TextBox();
            this.Input2 = new System.Windows.Forms.TextBox();
            this.Input3 = new System.Windows.Forms.TextBox();
            this.Input7 = new System.Windows.Forms.TextBox();
            this.Input8 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.RegisterButton = new projekt_po.RoundedButton();
            this.roundedButton1 = new projekt_po.RoundedButton();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox28.SuspendLayout();
            this.groupBox29.SuspendLayout();
            this.groupBox30.SuspendLayout();
            this.groupBox31.SuspendLayout();
            this.groupBox32.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radio1_2);
            this.groupBox2.Location = new System.Drawing.Point(410, 343);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(44, 36);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // radio1_2
            // 
            this.radio1_2.AutoSize = true;
            this.radio1_2.Location = new System.Drawing.Point(6, 11);
            this.radio1_2.Name = "radio1_2";
            this.radio1_2.Size = new System.Drawing.Size(14, 13);
            this.radio1_2.TabIndex = 0;
            this.radio1_2.TabStop = true;
            this.radio1_2.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radio1_1);
            this.groupBox3.Location = new System.Drawing.Point(410, 385);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(44, 36);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // radio1_1
            // 
            this.radio1_1.AutoSize = true;
            this.radio1_1.Location = new System.Drawing.Point(6, 11);
            this.radio1_1.Name = "radio1_1";
            this.radio1_1.Size = new System.Drawing.Size(14, 13);
            this.radio1_1.TabIndex = 0;
            this.radio1_1.TabStop = true;
            this.radio1_1.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.radio1_0);
            this.groupBox4.Location = new System.Drawing.Point(410, 427);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(44, 36);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // radio1_0
            // 
            this.radio1_0.AutoSize = true;
            this.radio1_0.Location = new System.Drawing.Point(6, 11);
            this.radio1_0.Name = "radio1_0";
            this.radio1_0.Size = new System.Drawing.Size(14, 13);
            this.radio1_0.TabIndex = 0;
            this.radio1_0.TabStop = true;
            this.radio1_0.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.radio2_0);
            this.groupBox5.Location = new System.Drawing.Point(471, 427);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(44, 36);
            this.groupBox5.TabIndex = 6;
            this.groupBox5.TabStop = false;
            // 
            // radio2_0
            // 
            this.radio2_0.AutoSize = true;
            this.radio2_0.Location = new System.Drawing.Point(6, 11);
            this.radio2_0.Name = "radio2_0";
            this.radio2_0.Size = new System.Drawing.Size(14, 13);
            this.radio2_0.TabIndex = 0;
            this.radio2_0.TabStop = true;
            this.radio2_0.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.radio2_1);
            this.groupBox6.Location = new System.Drawing.Point(471, 385);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(44, 36);
            this.groupBox6.TabIndex = 7;
            this.groupBox6.TabStop = false;
            // 
            // radio2_1
            // 
            this.radio2_1.AutoSize = true;
            this.radio2_1.Location = new System.Drawing.Point(6, 11);
            this.radio2_1.Name = "radio2_1";
            this.radio2_1.Size = new System.Drawing.Size(14, 13);
            this.radio2_1.TabIndex = 0;
            this.radio2_1.TabStop = true;
            this.radio2_1.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.radio2_2);
            this.groupBox7.Location = new System.Drawing.Point(471, 343);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(44, 36);
            this.groupBox7.TabIndex = 5;
            this.groupBox7.TabStop = false;
            // 
            // radio2_2
            // 
            this.radio2_2.AutoSize = true;
            this.radio2_2.Location = new System.Drawing.Point(6, 11);
            this.radio2_2.Name = "radio2_2";
            this.radio2_2.Size = new System.Drawing.Size(14, 13);
            this.radio2_2.TabIndex = 0;
            this.radio2_2.TabStop = true;
            this.radio2_2.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.radio2_3);
            this.groupBox8.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox8.Location = new System.Drawing.Point(471, 297);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(44, 40);
            this.groupBox8.TabIndex = 4;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "2";
            // 
            // radio2_3
            // 
            this.radio2_3.AutoSize = true;
            this.radio2_3.Location = new System.Drawing.Point(6, 19);
            this.radio2_3.Name = "radio2_3";
            this.radio2_3.Size = new System.Drawing.Size(14, 13);
            this.radio2_3.TabIndex = 0;
            this.radio2_3.TabStop = true;
            this.radio2_3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.radio4_0);
            this.groupBox9.Location = new System.Drawing.Point(594, 427);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(44, 36);
            this.groupBox9.TabIndex = 14;
            this.groupBox9.TabStop = false;
            // 
            // radio4_0
            // 
            this.radio4_0.AutoSize = true;
            this.radio4_0.Location = new System.Drawing.Point(6, 11);
            this.radio4_0.Name = "radio4_0";
            this.radio4_0.Size = new System.Drawing.Size(14, 13);
            this.radio4_0.TabIndex = 0;
            this.radio4_0.TabStop = true;
            this.radio4_0.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radio4_1);
            this.groupBox10.Location = new System.Drawing.Point(594, 385);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(44, 36);
            this.groupBox10.TabIndex = 15;
            this.groupBox10.TabStop = false;
            // 
            // radio4_1
            // 
            this.radio4_1.AutoSize = true;
            this.radio4_1.Location = new System.Drawing.Point(6, 11);
            this.radio4_1.Name = "radio4_1";
            this.radio4_1.Size = new System.Drawing.Size(14, 13);
            this.radio4_1.TabIndex = 0;
            this.radio4_1.TabStop = true;
            this.radio4_1.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.radio4_2);
            this.groupBox11.Location = new System.Drawing.Point(594, 343);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(44, 36);
            this.groupBox11.TabIndex = 13;
            this.groupBox11.TabStop = false;
            // 
            // radio4_2
            // 
            this.radio4_2.AutoSize = true;
            this.radio4_2.Location = new System.Drawing.Point(6, 11);
            this.radio4_2.Name = "radio4_2";
            this.radio4_2.Size = new System.Drawing.Size(14, 13);
            this.radio4_2.TabIndex = 0;
            this.radio4_2.TabStop = true;
            this.radio4_2.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.radio4_3);
            this.groupBox12.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox12.Location = new System.Drawing.Point(594, 297);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(44, 40);
            this.groupBox12.TabIndex = 12;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "4";
            // 
            // radio4_3
            // 
            this.radio4_3.AutoSize = true;
            this.radio4_3.Location = new System.Drawing.Point(6, 19);
            this.radio4_3.Name = "radio4_3";
            this.radio4_3.Size = new System.Drawing.Size(14, 13);
            this.radio4_3.TabIndex = 0;
            this.radio4_3.TabStop = true;
            this.radio4_3.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.radio3_0);
            this.groupBox13.Location = new System.Drawing.Point(533, 427);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(44, 36);
            this.groupBox13.TabIndex = 10;
            this.groupBox13.TabStop = false;
            // 
            // radio3_0
            // 
            this.radio3_0.AutoSize = true;
            this.radio3_0.Location = new System.Drawing.Point(6, 11);
            this.radio3_0.Name = "radio3_0";
            this.radio3_0.Size = new System.Drawing.Size(14, 13);
            this.radio3_0.TabIndex = 0;
            this.radio3_0.TabStop = true;
            this.radio3_0.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.radio3_1);
            this.groupBox14.Location = new System.Drawing.Point(533, 385);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(44, 36);
            this.groupBox14.TabIndex = 11;
            this.groupBox14.TabStop = false;
            // 
            // radio3_1
            // 
            this.radio3_1.AutoSize = true;
            this.radio3_1.Location = new System.Drawing.Point(6, 11);
            this.radio3_1.Name = "radio3_1";
            this.radio3_1.Size = new System.Drawing.Size(14, 13);
            this.radio3_1.TabIndex = 0;
            this.radio3_1.TabStop = true;
            this.radio3_1.UseVisualStyleBackColor = true;
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.radio3_2);
            this.groupBox15.Location = new System.Drawing.Point(533, 343);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(44, 36);
            this.groupBox15.TabIndex = 9;
            this.groupBox15.TabStop = false;
            // 
            // radio3_2
            // 
            this.radio3_2.AutoSize = true;
            this.radio3_2.Location = new System.Drawing.Point(6, 11);
            this.radio3_2.Name = "radio3_2";
            this.radio3_2.Size = new System.Drawing.Size(14, 13);
            this.radio3_2.TabIndex = 0;
            this.radio3_2.TabStop = true;
            this.radio3_2.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.radio3_3);
            this.groupBox16.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox16.Location = new System.Drawing.Point(533, 297);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(44, 40);
            this.groupBox16.TabIndex = 8;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "3";
            // 
            // radio3_3
            // 
            this.radio3_3.AutoSize = true;
            this.radio3_3.Location = new System.Drawing.Point(6, 19);
            this.radio3_3.Name = "radio3_3";
            this.radio3_3.Size = new System.Drawing.Size(14, 13);
            this.radio3_3.TabIndex = 0;
            this.radio3_3.TabStop = true;
            this.radio3_3.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(586, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 26);
            this.label1.TabIndex = 16;
            this.label1.Text = "Wpisz cyfry";
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.radio8_0);
            this.groupBox17.Location = new System.Drawing.Point(839, 427);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(44, 36);
            this.groupBox17.TabIndex = 32;
            this.groupBox17.TabStop = false;
            // 
            // radio8_0
            // 
            this.radio8_0.AutoSize = true;
            this.radio8_0.Location = new System.Drawing.Point(6, 11);
            this.radio8_0.Name = "radio8_0";
            this.radio8_0.Size = new System.Drawing.Size(14, 13);
            this.radio8_0.TabIndex = 0;
            this.radio8_0.TabStop = true;
            this.radio8_0.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.radio8_1);
            this.groupBox18.Location = new System.Drawing.Point(839, 385);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(44, 36);
            this.groupBox18.TabIndex = 33;
            this.groupBox18.TabStop = false;
            // 
            // radio8_1
            // 
            this.radio8_1.AutoSize = true;
            this.radio8_1.Location = new System.Drawing.Point(6, 11);
            this.radio8_1.Name = "radio8_1";
            this.radio8_1.Size = new System.Drawing.Size(14, 13);
            this.radio8_1.TabIndex = 0;
            this.radio8_1.TabStop = true;
            this.radio8_1.UseVisualStyleBackColor = true;
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.radio8_2);
            this.groupBox19.Location = new System.Drawing.Point(839, 343);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(44, 36);
            this.groupBox19.TabIndex = 31;
            this.groupBox19.TabStop = false;
            // 
            // radio8_2
            // 
            this.radio8_2.AutoSize = true;
            this.radio8_2.Location = new System.Drawing.Point(6, 11);
            this.radio8_2.Name = "radio8_2";
            this.radio8_2.Size = new System.Drawing.Size(14, 13);
            this.radio8_2.TabIndex = 0;
            this.radio8_2.TabStop = true;
            this.radio8_2.UseVisualStyleBackColor = true;
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.radio8_3);
            this.groupBox20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox20.Location = new System.Drawing.Point(839, 297);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(44, 40);
            this.groupBox20.TabIndex = 30;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "8";
            // 
            // radio8_3
            // 
            this.radio8_3.AutoSize = true;
            this.radio8_3.Location = new System.Drawing.Point(6, 19);
            this.radio8_3.Name = "radio8_3";
            this.radio8_3.Size = new System.Drawing.Size(14, 13);
            this.radio8_3.TabIndex = 0;
            this.radio8_3.TabStop = true;
            this.radio8_3.UseVisualStyleBackColor = true;
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.radio7_0);
            this.groupBox21.Location = new System.Drawing.Point(778, 427);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(44, 36);
            this.groupBox21.TabIndex = 28;
            this.groupBox21.TabStop = false;
            // 
            // radio7_0
            // 
            this.radio7_0.AutoSize = true;
            this.radio7_0.Location = new System.Drawing.Point(6, 11);
            this.radio7_0.Name = "radio7_0";
            this.radio7_0.Size = new System.Drawing.Size(14, 13);
            this.radio7_0.TabIndex = 0;
            this.radio7_0.TabStop = true;
            this.radio7_0.UseVisualStyleBackColor = true;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.radio7_1);
            this.groupBox22.Location = new System.Drawing.Point(778, 385);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(44, 36);
            this.groupBox22.TabIndex = 29;
            this.groupBox22.TabStop = false;
            // 
            // radio7_1
            // 
            this.radio7_1.AutoSize = true;
            this.radio7_1.Location = new System.Drawing.Point(6, 11);
            this.radio7_1.Name = "radio7_1";
            this.radio7_1.Size = new System.Drawing.Size(14, 13);
            this.radio7_1.TabIndex = 0;
            this.radio7_1.TabStop = true;
            this.radio7_1.UseVisualStyleBackColor = true;
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.radio7_2);
            this.groupBox23.Location = new System.Drawing.Point(778, 343);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(44, 36);
            this.groupBox23.TabIndex = 27;
            this.groupBox23.TabStop = false;
            // 
            // radio7_2
            // 
            this.radio7_2.AutoSize = true;
            this.radio7_2.Location = new System.Drawing.Point(6, 11);
            this.radio7_2.Name = "radio7_2";
            this.radio7_2.Size = new System.Drawing.Size(14, 13);
            this.radio7_2.TabIndex = 0;
            this.radio7_2.TabStop = true;
            this.radio7_2.UseVisualStyleBackColor = true;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.radio7_3);
            this.groupBox24.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox24.Location = new System.Drawing.Point(778, 297);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(44, 40);
            this.groupBox24.TabIndex = 26;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "7";
            // 
            // radio7_3
            // 
            this.radio7_3.AutoSize = true;
            this.radio7_3.Location = new System.Drawing.Point(6, 19);
            this.radio7_3.Name = "radio7_3";
            this.radio7_3.Size = new System.Drawing.Size(14, 13);
            this.radio7_3.TabIndex = 0;
            this.radio7_3.TabStop = true;
            this.radio7_3.UseVisualStyleBackColor = true;
            // 
            // groupBox25
            // 
            this.groupBox25.Controls.Add(this.radio6_0);
            this.groupBox25.Location = new System.Drawing.Point(716, 427);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(44, 36);
            this.groupBox25.TabIndex = 24;
            this.groupBox25.TabStop = false;
            // 
            // radio6_0
            // 
            this.radio6_0.AutoSize = true;
            this.radio6_0.Location = new System.Drawing.Point(6, 11);
            this.radio6_0.Name = "radio6_0";
            this.radio6_0.Size = new System.Drawing.Size(14, 13);
            this.radio6_0.TabIndex = 0;
            this.radio6_0.TabStop = true;
            this.radio6_0.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.radio6_1);
            this.groupBox26.Location = new System.Drawing.Point(716, 385);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(44, 36);
            this.groupBox26.TabIndex = 25;
            this.groupBox26.TabStop = false;
            // 
            // radio6_1
            // 
            this.radio6_1.AutoSize = true;
            this.radio6_1.Location = new System.Drawing.Point(6, 11);
            this.radio6_1.Name = "radio6_1";
            this.radio6_1.Size = new System.Drawing.Size(14, 13);
            this.radio6_1.TabIndex = 0;
            this.radio6_1.TabStop = true;
            this.radio6_1.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.radio6_2);
            this.groupBox27.Location = new System.Drawing.Point(716, 343);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(44, 36);
            this.groupBox27.TabIndex = 23;
            this.groupBox27.TabStop = false;
            // 
            // radio6_2
            // 
            this.radio6_2.AutoSize = true;
            this.radio6_2.Location = new System.Drawing.Point(6, 11);
            this.radio6_2.Name = "radio6_2";
            this.radio6_2.Size = new System.Drawing.Size(14, 13);
            this.radio6_2.TabIndex = 0;
            this.radio6_2.TabStop = true;
            this.radio6_2.UseVisualStyleBackColor = true;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.radio6_3);
            this.groupBox28.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox28.Location = new System.Drawing.Point(716, 297);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(44, 40);
            this.groupBox28.TabIndex = 22;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "6";
            // 
            // radio6_3
            // 
            this.radio6_3.AutoSize = true;
            this.radio6_3.Location = new System.Drawing.Point(6, 19);
            this.radio6_3.Name = "radio6_3";
            this.radio6_3.Size = new System.Drawing.Size(14, 13);
            this.radio6_3.TabIndex = 0;
            this.radio6_3.TabStop = true;
            this.radio6_3.UseVisualStyleBackColor = true;
            // 
            // groupBox29
            // 
            this.groupBox29.Controls.Add(this.radio5_0);
            this.groupBox29.Location = new System.Drawing.Point(655, 427);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(44, 36);
            this.groupBox29.TabIndex = 20;
            this.groupBox29.TabStop = false;
            // 
            // radio5_0
            // 
            this.radio5_0.AutoSize = true;
            this.radio5_0.Location = new System.Drawing.Point(6, 11);
            this.radio5_0.Name = "radio5_0";
            this.radio5_0.Size = new System.Drawing.Size(14, 13);
            this.radio5_0.TabIndex = 0;
            this.radio5_0.TabStop = true;
            this.radio5_0.UseVisualStyleBackColor = true;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.radio5_1);
            this.groupBox30.Location = new System.Drawing.Point(655, 385);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(44, 36);
            this.groupBox30.TabIndex = 21;
            this.groupBox30.TabStop = false;
            // 
            // radio5_1
            // 
            this.radio5_1.AutoSize = true;
            this.radio5_1.Location = new System.Drawing.Point(6, 11);
            this.radio5_1.Name = "radio5_1";
            this.radio5_1.Size = new System.Drawing.Size(14, 13);
            this.radio5_1.TabIndex = 0;
            this.radio5_1.TabStop = true;
            this.radio5_1.UseVisualStyleBackColor = true;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.radio5_2);
            this.groupBox31.Location = new System.Drawing.Point(655, 343);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(44, 36);
            this.groupBox31.TabIndex = 19;
            this.groupBox31.TabStop = false;
            // 
            // radio5_2
            // 
            this.radio5_2.AutoSize = true;
            this.radio5_2.Location = new System.Drawing.Point(6, 11);
            this.radio5_2.Name = "radio5_2";
            this.radio5_2.Size = new System.Drawing.Size(14, 13);
            this.radio5_2.TabIndex = 0;
            this.radio5_2.TabStop = true;
            this.radio5_2.UseVisualStyleBackColor = true;
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.radio5_3);
            this.groupBox32.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox32.Location = new System.Drawing.Point(655, 297);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(44, 40);
            this.groupBox32.TabIndex = 18;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "5";
            // 
            // radio5_3
            // 
            this.radio5_3.AutoSize = true;
            this.radio5_3.Location = new System.Drawing.Point(6, 19);
            this.radio5_3.Name = "radio5_3";
            this.radio5_3.Size = new System.Drawing.Size(14, 13);
            this.radio5_3.TabIndex = 0;
            this.radio5_3.TabStop = true;
            this.radio5_3.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radio1_3);
            this.groupBox1.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.groupBox1.Location = new System.Drawing.Point(410, 297);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(44, 40);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "1";
            // 
            // radio1_3
            // 
            this.radio1_3.AutoSize = true;
            this.radio1_3.Location = new System.Drawing.Point(6, 19);
            this.radio1_3.Name = "radio1_3";
            this.radio1_3.Size = new System.Drawing.Size(14, 13);
            this.radio1_3.TabIndex = 0;
            this.radio1_3.TabStop = true;
            this.radio1_3.UseVisualStyleBackColor = true;
            // 
            // Input4
            // 
            this.Input4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input4.Location = new System.Drawing.Point(589, 116);
            this.Input4.MaxLength = 2;
            this.Input4.Name = "Input4";
            this.Input4.Size = new System.Drawing.Size(30, 31);
            this.Input4.TabIndex = 38;
            this.Input4.TextChanged += new System.EventHandler(this.InputTextChanged);
            // 
            // Input5
            // 
            this.Input5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input5.Location = new System.Drawing.Point(632, 116);
            this.Input5.MaxLength = 2;
            this.Input5.Name = "Input5";
            this.Input5.Size = new System.Drawing.Size(30, 31);
            this.Input5.TabIndex = 39;
            this.Input5.TextChanged += new System.EventHandler(this.InputTextChanged);
            // 
            // Input6
            // 
            this.Input6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input6.Location = new System.Drawing.Point(680, 116);
            this.Input6.MaxLength = 2;
            this.Input6.Name = "Input6";
            this.Input6.Size = new System.Drawing.Size(30, 31);
            this.Input6.TabIndex = 40;
            this.Input6.TextChanged += new System.EventHandler(this.InputTextChanged);
            // 
            // Input1
            // 
            this.Input1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input1.Location = new System.Drawing.Point(405, 116);
            this.Input1.Name = "Input1";
            this.Input1.ReadOnly = true;
            this.Input1.Size = new System.Drawing.Size(30, 31);
            this.Input1.TabIndex = 35;
            // 
            // Input2
            // 
            this.Input2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input2.Location = new System.Drawing.Point(441, 116);
            this.Input2.Name = "Input2";
            this.Input2.ReadOnly = true;
            this.Input2.Size = new System.Drawing.Size(30, 31);
            this.Input2.TabIndex = 36;
            // 
            // Input3
            // 
            this.Input3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input3.Location = new System.Drawing.Point(477, 116);
            this.Input3.Name = "Input3";
            this.Input3.ReadOnly = true;
            this.Input3.Size = new System.Drawing.Size(30, 31);
            this.Input3.TabIndex = 37;
            // 
            // Input7
            // 
            this.Input7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input7.Location = new System.Drawing.Point(792, 116);
            this.Input7.MaxLength = 2;
            this.Input7.Name = "Input7";
            this.Input7.Size = new System.Drawing.Size(30, 31);
            this.Input7.TabIndex = 41;
            this.Input7.TextChanged += new System.EventHandler(this.InputTextChanged);
            // 
            // Input8
            // 
            this.Input8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Input8.Location = new System.Drawing.Point(828, 116);
            this.Input8.MaxLength = 2;
            this.Input8.Name = "Input8";
            this.Input8.Size = new System.Drawing.Size(30, 31);
            this.Input8.TabIndex = 42;
            this.Input8.TextChanged += new System.EventHandler(this.InputTextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(355, 438);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "2^0=1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(355, 396);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "2^1=2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(355, 354);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "2^2=4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(355, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "2^3=8";
            // 
            // RegisterButton
            // 
            this.RegisterButton.BackColor = System.Drawing.SystemColors.HighlightText;
            this.RegisterButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.RegisterButton.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RegisterButton.Location = new System.Drawing.Point(1018, 73);
            this.RegisterButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(165, 49);
            this.RegisterButton.TabIndex = 47;
            this.RegisterButton.Text = "Zrzut okna";
            this.RegisterButton.UseVisualStyleBackColor = false;
            this.RegisterButton.Click += new System.EventHandler(this.ScreenshotButton_Click);
            // 
            // roundedButton1
            // 
            this.roundedButton1.BackColor = System.Drawing.SystemColors.HighlightText;
            this.roundedButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundedButton1.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.roundedButton1.ForeColor = System.Drawing.Color.Red;
            this.roundedButton1.Location = new System.Drawing.Point(1018, 12);
            this.roundedButton1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.roundedButton1.Name = "roundedButton1";
            this.roundedButton1.Size = new System.Drawing.Size(165, 49);
            this.roundedButton1.TabIndex = 48;
            this.roundedButton1.Text = "Wyloguj";
            this.roundedButton1.UseVisualStyleBackColor = false;
            this.roundedButton1.Click += new System.EventHandler(this.MainToLoginButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1199, 595);
            this.Controls.Add(this.roundedButton1);
            this.Controls.Add(this.RegisterButton);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.Input8);
            this.Controls.Add(this.Input7);
            this.Controls.Add(this.Input3);
            this.Controls.Add(this.Input2);
            this.Controls.Add(this.Input1);
            this.Controls.Add(this.Input6);
            this.Controls.Add(this.Input5);
            this.Controls.Add(this.Input4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox17);
            this.Controls.Add(this.groupBox18);
            this.Controls.Add(this.groupBox19);
            this.Controls.Add(this.groupBox20);
            this.Controls.Add(this.groupBox21);
            this.Controls.Add(this.groupBox22);
            this.Controls.Add(this.groupBox23);
            this.Controls.Add(this.groupBox24);
            this.Controls.Add(this.groupBox25);
            this.Controls.Add(this.groupBox26);
            this.Controls.Add(this.groupBox27);
            this.Controls.Add(this.groupBox28);
            this.Controls.Add(this.groupBox29);
            this.Controls.Add(this.groupBox30);
            this.Controls.Add(this.groupBox31);
            this.Controls.Add(this.groupBox32);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox11);
            this.Controls.Add(this.groupBox12);
            this.Controls.Add(this.groupBox13);
            this.Controls.Add(this.groupBox14);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.groupBox16);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            this.groupBox29.PerformLayout();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radio1_2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radio1_1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton radio1_0;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton radio2_0;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton radio2_1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.RadioButton radio2_2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton radio2_3;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton radio4_0;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton radio4_1;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.RadioButton radio4_2;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton radio4_3;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.RadioButton radio3_0;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.RadioButton radio3_1;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.RadioButton radio3_2;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton radio3_3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.RadioButton radio8_0;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.RadioButton radio8_1;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.RadioButton radio8_2;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.RadioButton radio8_3;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.RadioButton radio7_0;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.RadioButton radio7_1;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.RadioButton radio7_2;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.RadioButton radio7_3;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.RadioButton radio6_0;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.RadioButton radio6_1;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.RadioButton radio6_2;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.RadioButton radio6_3;
        private System.Windows.Forms.GroupBox groupBox29;
        private System.Windows.Forms.RadioButton radio5_0;
        private System.Windows.Forms.GroupBox groupBox30;
        private System.Windows.Forms.RadioButton radio5_1;
        private System.Windows.Forms.GroupBox groupBox31;
        private System.Windows.Forms.RadioButton radio5_2;
        private System.Windows.Forms.GroupBox groupBox32;
        private System.Windows.Forms.RadioButton radio5_3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radio1_3;
        private System.Windows.Forms.TextBox Input4;
        private System.Windows.Forms.TextBox Input5;
        private System.Windows.Forms.TextBox Input6;
        private System.Windows.Forms.TextBox Input1;
        private System.Windows.Forms.TextBox Input2;
        private System.Windows.Forms.TextBox Input3;
        private System.Windows.Forms.TextBox Input7;
        private System.Windows.Forms.TextBox Input8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private RoundedButton RegisterButton;
        private RoundedButton roundedButton1;
    }
}