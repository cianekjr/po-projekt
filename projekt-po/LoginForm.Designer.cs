﻿namespace projekt_po
{
    partial class LoginForm
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.LoginTitle = new System.Windows.Forms.Label();
            this.LoginToRegisterLink = new System.Windows.Forms.LinkLabel();
            this.ErrorBox = new System.Windows.Forms.Label();
            this.LoginButton = new projekt_po.RoundedButton();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginInputPassword = new projekt_po.BorderInput();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LoginInputId = new projekt_po.BorderInput();
            this.SuspendLayout();
            // 
            // LoginTitle
            // 
            this.LoginTitle.AutoSize = true;
            this.LoginTitle.Font = new System.Drawing.Font("Verdana", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LoginTitle.Location = new System.Drawing.Point(504, 59);
            this.LoginTitle.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LoginTitle.Name = "LoginTitle";
            this.LoginTitle.Size = new System.Drawing.Size(234, 32);
            this.LoginTitle.TabIndex = 0;
            this.LoginTitle.Text = "Witaj ponownie!";
            // 
            // LoginToRegisterLink
            // 
            this.LoginToRegisterLink.AutoSize = true;
            this.LoginToRegisterLink.Font = new System.Drawing.Font("Verdana", 10.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LoginToRegisterLink.Location = new System.Drawing.Point(629, 426);
            this.LoginToRegisterLink.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.LoginToRegisterLink.Name = "LoginToRegisterLink";
            this.LoginToRegisterLink.Size = new System.Drawing.Size(115, 18);
            this.LoginToRegisterLink.TabIndex = 4;
            this.LoginToRegisterLink.TabStop = true;
            this.LoginToRegisterLink.Text = "Zarejestuj się!";
            this.LoginToRegisterLink.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LoginToRegisterLink_LinkClicked);
            // 
            // ErrorBox
            // 
            this.ErrorBox.AutoSize = true;
            this.ErrorBox.BackColor = System.Drawing.SystemColors.Highlight;
            this.ErrorBox.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ErrorBox.ForeColor = System.Drawing.Color.Red;
            this.ErrorBox.Location = new System.Drawing.Point(446, 303);
            this.ErrorBox.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.ErrorBox.Name = "ErrorBox";
            this.ErrorBox.Size = new System.Drawing.Size(0, 17);
            this.ErrorBox.TabIndex = 5;
            // 
            // LoginButton
            // 
            this.LoginButton.BackColor = System.Drawing.Color.White;
            this.LoginButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LoginButton.Font = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LoginButton.Location = new System.Drawing.Point(463, 346);
            this.LoginButton.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.LoginButton.Name = "LoginButton";
            this.LoginButton.Size = new System.Drawing.Size(301, 57);
            this.LoginButton.TabIndex = 6;
            this.LoginButton.Text = "Zaloguj";
            this.LoginButton.UseVisualStyleBackColor = false;
            this.LoginButton.Click += new System.EventHandler(this.LoginButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 10.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(486, 426);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Nie masz konta?";
            // 
            // LoginInputPassword
            // 
            this.LoginInputPassword.BackColor = System.Drawing.SystemColors.Highlight;
            this.LoginInputPassword.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LoginInputPassword.Location = new System.Drawing.Point(463, 221);
            this.LoginInputPassword.Name = "LoginInputPassword";
            this.LoginInputPassword.Size = new System.Drawing.Size(301, 33);
            this.LoginInputPassword.TabIndex = 9;
            this.LoginInputPassword.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(716, 257);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 10;
            this.label2.Text = "Hasło";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label3.Location = new System.Drawing.Point(666, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Identyfikator";
            // 
            // LoginInputId
            // 
            this.LoginInputId.BackColor = System.Drawing.SystemColors.Highlight;
            this.LoginInputId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.LoginInputId.Location = new System.Drawing.Point(463, 133);
            this.LoginInputId.MaxLength = 3;
            this.LoginInputId.Name = "LoginInputId";
            this.LoginInputId.Size = new System.Drawing.Size(301, 33);
            this.LoginInputId.TabIndex = 8;
            // 
            // LoginForm
            // 
            this.AcceptButton = this.LoginButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.ClientSize = new System.Drawing.Size(1199, 593);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.LoginInputPassword);
            this.Controls.Add(this.LoginInputId);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LoginButton);
            this.Controls.Add(this.ErrorBox);
            this.Controls.Add(this.LoginToRegisterLink);
            this.Controls.Add(this.LoginTitle);
            this.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "LoginForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LoginTitle;
        private System.Windows.Forms.LinkLabel LoginToRegisterLink;
        private System.Windows.Forms.Label ErrorBox;
        private RoundedButton LoginButton;
        private System.Windows.Forms.Label label1;
        private BorderInput LoginInputPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private BorderInput LoginInputId;
    }
}

