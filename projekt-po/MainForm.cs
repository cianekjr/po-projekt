﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace projekt_po
{
    public partial class MainForm : Form
    {
        public MainForm(string userId)
        {
            InitializeComponent();
            UserIdUpdate(userId);
            int[] inputNumbers = GetAllInputNumbers();
            AllRadiosUpdate(inputNumbers);

        }
        public int[] IntToBin(int dec, int numBits = 4) // 9 => [1, 0, 0, 1]
        {
            int[] bin = new int[4];
            for (int i = 0; i < numBits; i++)
            {
                bin[i] = 1 & (dec >> i);
            }
            return bin;
        }
        public bool[] GetBoolArrFromBin(int[] arr)  // [1, 0, 0, 1] => [true, false, false, true]
        {
            bool[] result = new bool[4];

            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] == 1)
                {
                    result[i] = true;
                }
                else
                {
                    result[i] = false;
                }
            }
            return result;
        }
        public void SingleRadioUpdate(bool[] binArray, string name)
        {
            for (int i = 0; i <= 3; i++)
            {
                RadioButton radio = (RadioButton)this.Controls.Find(name + "_" + Convert.ToString(i), true)[0];
                radio.Checked = binArray[i];
            }
        }
        public void AllRadiosUpdate(int[] numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                string name = "radio" + Convert.ToString(i + 1);
                int[] binary = IntToBin(Convert.ToInt32(numbers[i]));
                bool[] boolArray = GetBoolArrFromBin(binary);
                SingleRadioUpdate(boolArray, name);
            }
        }
        public int[] GetAllInputNumbers()
        {
            int[] inputNumbers = new int[8];
            for (int i = 0; i <= 7; i++)
            {
                TextBox input = (TextBox)this.Controls.Find("input" + Convert.ToString(i + 1), true)[0];
                if (input.Text != "" && input.Text.All(char.IsDigit) && Convert.ToInt32(input.Text) >= 0 && Convert.ToInt32(input.Text) <= 9)
                {
                    input.BackColor = Color.White;
                    inputNumbers[i] = Convert.ToInt32(input.Text);
                }
                else
                {
                    input.BackColor = Color.LightCoral;
                    inputNumbers[i] = 0;
                }
            }
            return inputNumbers;
        }
        private void InputTextChanged(object sender, EventArgs e)
        {
            int[] inputNumbers = GetAllInputNumbers();
            AllRadiosUpdate(inputNumbers);
        }
        private void UserIdUpdate(string userId)
        {
            for (int i = 0; i < 3; i++)
            {
                TextBox input = (TextBox)this.Controls.Find("Input" + Convert.ToString(i + 1), true)[0];
                input.Text = Convert.ToString(userId[i]);
            }
        }

        private void MainToLoginButton_Click(object sender, EventArgs e)
        {
            RedirectToLogin();
        }
        private void RedirectToLogin()
        {
            LoginForm loginForm = new LoginForm
            {
                Width = this.Width,
                Height = this.Height,
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.Location.X, this.Location.Y)
            };
            this.Hide();
            loginForm.ShowDialog();
            this.Close();
        }

        private void ScreenshotButton_Click(object sender, EventArgs e)
        {
            MakeScreenshot();
        }
        private void MakeScreenshot()
        {
            var frm = Form.ActiveForm;
            using (var bmp = new Bitmap(frm.Width, frm.Height))
            {
                string path = DateTime.Now.ToString("dd-MM-yyyy-HH;mm;ss;fff") + ".png";
                frm.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                bmp.Save(path);
            }
        }
    }
}
