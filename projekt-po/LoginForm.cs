﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace projekt_po
{
    public partial class LoginForm : Form
    {
        public LoginForm(string id = "")
        {
            InitializeComponent();
            LoginInputId.Text = id;
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void LoginToRegisterLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            RedirectToRegister();
        }
        private void RedirectToRegister()
        {
            RegisterForm registerForm = new RegisterForm
            {
                Width = this.Width,
                Height = this.Height,
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.Location.X, this.Location.Y)
            };
            this.Hide();
            registerForm.ShowDialog();
            this.Close();
        }
        private void RedirectToMain(string userId)
        {
            MainForm mainForm = new MainForm(userId)
            {
                Width = this.Width,
                Height = this.Height,
                StartPosition = FormStartPosition.Manual,
                Location = new Point(this.Location.X, this.Location.Y)
            };
            this.Hide();
            mainForm.ShowDialog();
            this.Close();
        }
        private void Login()
        {
            string id = LoginInputId.Text;
            string password = LoginInputPassword.Text;

            string path = "users.txt";

            if (File.Exists(path))
            {
                string[] fileText = File.ReadAllLines(path);

                foreach (string line in fileText)
                {
                    string[] words = line.Split(' ');
                    bool isValidId = words[0] == id;
                    bool isValidPassword = PasswordHandler.Validate(password, words[1]);

                    if (isValidId && isValidPassword)
                    {
                        RedirectToMain(id);
                    }
                    else
                    {
                        ErrorBox.Text = "Nieprawidłowy id lub hasło. Spróbuj ponownie!";
                    }
                }
            }
            else
            {
                ErrorBox.Text = "Baza z użytkownikami jest pusta. Zarejestruj się!";
            }
        }
    }
}
